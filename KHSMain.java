package Tugas_3_PemlanTeori;

// Nama: Raya Abiathar
// Kelas: Sistem Informasi A
// NIM: 235150400111001

import java.util.ArrayList;
import java.util.Scanner;

class Mahasiswa {
    
    private String NIM_Mahasiswa;
    private String Nama_Mahasiswa;
    private ArrayList<MataKuliah> mataKuliahs = new ArrayList<>();
    
    public ArrayList<MataKuliah> getMatkuls() {
        return mataKuliahs;
    }

    public void setMatkuls(ArrayList<MataKuliah> mataKuliahs) {
        this.mataKuliahs = mataKuliahs;
    }

    public String getNIM_Mahasiswa() {
        return NIM_Mahasiswa;
    }

    public void setNIM_Mahasiswa(String nimMahasiswa) {
        this.NIM_Mahasiswa = nimMahasiswa;
    }

    public String getNama_Mahasiswa() {
        return Nama_Mahasiswa;
    }

    public void setNama_Mahasiswa(String namaMahasiswa) {
        this.Nama_Mahasiswa = namaMahasiswa;
    }
}

class MataKuliah {

    private float valueAngka;
    private char valueHuruf;
    private String Kode_Matkul;
    private String Nama_Matkul;

    public String getKode_Matkul() {
        return Kode_Matkul;
    }

    public String getNama_Matkul() {
        return Nama_Matkul;
    }

    public void setNama_Matkul(String namaMataKul) {
        this.Nama_Matkul = namaMataKul;
    }

    public void setValueAngka(float valueAngka) {
        this.valueAngka = valueAngka;
    }

    public void setKode_Matkul(String kodeMatkul) {
        this.Kode_Matkul = kodeMatkul;
        
    }
    
    public char getValueHuruf() {
        int temp = (int)valueAngka;
        if (temp > 80) {
            valueHuruf = 'A';
        }
        else if (temp > 60) {
            valueHuruf = 'B';
        }
        else if (temp > 50) {
            valueHuruf = 'C';
        }
        else if (temp > 40) {
            valueHuruf = 'D';
        }
        else {
            valueHuruf = 'E';
        }
        return valueHuruf;
    }

}

public class KHSMain {
    public static void main(String[] args) {
        ArrayList<Mahasiswa> MHSWs = new ArrayList<>();

        Scanner input = new Scanner(System.in);
        boolean mahasiswaSelanjutnya = true;
        while (mahasiswaSelanjutnya) {
  
            System.out.print("Masukkan nama anda: ");
            String Nama_Mahasiswa = input.nextLine();

            System.out.print("Masukkan NIM anda: ");
            String NIM_Mahasiswa = input.nextLine();

            Mahasiswa mahasiswa = new Mahasiswa();
            mahasiswa.setNama_Mahasiswa(Nama_Mahasiswa);
            mahasiswa.setNIM_Mahasiswa(NIM_Mahasiswa);

            ArrayList<MataKuliah> MTKLs = new ArrayList<>();
            boolean nextMataKuliah = true;
            while (nextMataKuliah) {
                System.out.print("Masukkan Nama Matkul: ");
                String Nama_MataKuliah = input.nextLine();

                System.out.print("Masukkan Kode Matkul: ");
                String Kode_MataKuliah = input.nextLine();

                System.out.print("Masukkan Nilai Nomor Matkul: ");
                float Nilai_MataKuliah = input.nextFloat();

                MataKuliah mataKuliah = new MataKuliah();
                mataKuliah.setKode_Matkul(Kode_MataKuliah);
                mataKuliah.setNama_Matkul(Nama_MataKuliah);
                mataKuliah.setValueAngka(Nilai_MataKuliah);
                MTKLs.add(mataKuliah);
                input.nextLine();
                System.out.print("Apakah ada matkul lain? (Ketik \"y\" jika Ya, ketik \"t\" jika Tidak): ");
                String tambahMataKuliah = input.nextLine();

                if (tambahMataKuliah.equals("t")) {
                    nextMataKuliah = false;
                }
            }

            mahasiswa.setMatkuls(MTKLs);
            MHSWs.add(mahasiswa);
            System.out.print("Apakah ada mahasiswa lain? (Ketik \"y\" jika Ya, ketik \"t\" jika Tidak): ");
            String tambahMahasiswa = input.nextLine();

            if (tambahMahasiswa.equals("t")) {
                mahasiswaSelanjutnya = false;
            }
        }

        for (int i = 0; i < MHSWs.size(); i++) {
            System.out.println("============================================================================");
            System.out.println("");
            Mahasiswa mahasiswa = (Mahasiswa) MHSWs.get(i);
            System.out.println("Nama: " + mahasiswa.getNama_Mahasiswa());
            System.out.println("NIM: " + mahasiswa.getNIM_Mahasiswa());
            System.out.println("============================================================================");
            System.out.println("");
            ArrayList<MataKuliah> mataKuliahs = mahasiswa.getMatkuls();
            for (int j = 0; j < mataKuliahs.size(); j++ ) {
                MataKuliah mtKuliah = (MataKuliah) mataKuliahs.get(j);
                System.out.println("Kode Matkul: " + mtKuliah.getKode_Matkul() + " | Nama Matkul: " + mtKuliah.getNama_Matkul() + " | Nilai: " + mtKuliah.getValueHuruf());
            }
            System.out.println("============================================================================");            
            System.out.println();
            System.out.println();
        }
        input.close();
    }
    
}


